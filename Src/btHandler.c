// HC-05 driver
//-----------------------
// Author: Pavol Dudak
//
// September 2015
//      first version
//
// MIT license
// For STM32F030
//
// Documentation:
//  https://developer.mbed.org/users/edodm85/notebook/HC-05-bluetooth/

#include "stm32f0xx_hal.h"
#include "btHandler.h"

extern UART_HandleTypeDef huart1;

int8_t cmdSize;
uint8_t USARTrecBuff[5];
uint8_t bluetoothDataBuffer[20];
uint8_t cmdLen;
uint8_t inAT;
uint8_t AT_RESET[8] = {'A', 'T', '+', 'R', 'E', 'S', 'E', 'T'};

HC05_Result_t HC05_initBt(UART_HandleTypeDef *huart)
{
    huart1 = *huart;
    return HC05_Result_Ok;
}

HC05_Result_t HC05_getState(HC05_t *DataStruct)
{
    DataStruct->HC05_State = HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_1);
    return HC05_Result_Ok;
}

HC05_Result_t HC05_setAT()
{
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, 1);
    return HC05_Result_Ok;
}

HC05_Result_t HC05_checkCommand()
{
    if(HAL_UART_Receive(&huart1, USARTrecBuff, 1, 10) == HAL_OK)
    {
        switch (USARTrecBuff[0])
        {
        case BT_ENTER_AT:
            HC05_setAT();
            break;
        case BT_CMD_INC:
            HAL_UART_Receive(&huart1, USARTrecBuff, 1, 10);
            uint8_t cmdLen = USARTrecBuff[0];
            HAL_UART_Receive(&huart1, bluetoothDataBuffer, cmdLen, 10);
            HAL_UART_Transmit(&huart1, bluetoothDataBuffer, cmdLen, 10);
            break;
        case BT_EXIT_AT:
            HAL_UART_Transmit(&huart1, AT_RESET, 8, 10);
            HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, 0);
            break;
        default:
            return BT_UNKNOWN_CMD;
            break;
        }
        return HC05_Result_Ok;
    }
    return HC05_Result_DeviceNotConnected;
}

HC05_Result_t HC05_receiveCMD()
{
    HAL_UART_Receive(&huart1, USARTrecBuff, 1, 10);
    uint8_t cmdLen = USARTrecBuff[0];
    HAL_UART_Receive(&huart1, bluetoothDataBuffer, cmdLen, 10);
    return HC05_Result_Ok;
}

HC05_Result_t HC05_writeCmd()
{
    HAL_UART_Transmit(&huart1, bluetoothDataBuffer, cmdLen, 10);
    return HC05_Result_Ok;
}
