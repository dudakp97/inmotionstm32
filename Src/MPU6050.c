// MPU-9150/6050 library
//-----------------------
// Author: Pavol Dudak
//
// May 2015
//      first version
//      4.5.2015
//      getData() changed to getMotion6()
//      added getAcceleration()
//      added getRotation()
//      added getTemperature()
// December 2015
//      MPU6050_set_AccelRange();
//      MPU6050_set_DLPF();
//      MPU6050_set_MWR_MGMT1();
//      MPU6050_setSMPRT_DIV();
// MIT license
// For STM32F030
//
// Documentation:
//      "MPU-9150 Register Map and Descriptions Revision 4.2"
//      "MPU-6000 and MPU-6050 Register Map and Descriptions Revision 3.4"
//      "MPU-6000 and MPU-6050 Product Specification Revision 3.4"

#include "MPU6050.h"
#include "stm32f0xx_hal.h"

extern MPU6050_driver_typeDef MPU6050_driver;

uint8_t data[2];
uint8_t readings[14];
uint16_t t_read;
uint8_t I2Cdata[2];
int16_t calibVals[2];
int8_t motionIntStatus;
/**
* @brief Initializes MPU-6050.
* @param hi2c1 : Pointer to a I2C_HandleTypeDef structure that contains
*                the configuration information for the specified I2C.
* @param DataStruct : pointer to MPU6050_t struct
* @return MPU6050_Result_t
*/

MPU6050_Result_t MPU6050_Init(I2C_HandleTypeDef *hi2c, MPU6050_t *DataStruct, uint8_t addr)
{
    MPU6050_driver.i2c = hi2c;
    MPU6050_driver.Address = addr;
    DataStruct->acc_range = BIT_ACC_RANGE_2G;
    DataStruct->gyro_range = BIT_GYRO_RANGE_250;
    HAL_StatusTypeDef hs;

    hs = HAL_I2C_IsDeviceReady(MPU6050_driver.i2c, MPU6050_driver.Address, 200,10);
    if(hs==HAL_OK)
    {
        //setLowPower(clkSrc); // *
        MPU6050_setClockSource(BIT_CLK_PLL_XGYRO);
        MPU6050_setAccelRange(DataStruct->acc_range); // *
        MPU6050_setGyroRange(DataStruct->gyro_range); // *
        MPU6050_setSamplingRate(); //1kHZ default
        MPU6050_setDLPF(); //
        MPU6050_enableDataReadyInt(); //
        MPU6050_setHWInt(); //
    }

    return hs;
}

MPU6050_Result_t MPU6050_Init_ext(I2C_HandleTypeDef *hi2c, MPU6050_t *DataStruct, uint8_t addr, uint8_t clkSrc, uint8_t accelRange, uint8_t gyroRange)
{
    MPU6050_driver.i2c = hi2c;
    HAL_StatusTypeDef hs;


    MPU6050_driver.Address = addr;

    int i;
    for (i = 0; i < 10; i++)
    {
        hs = HAL_I2C_IsDeviceReady(MPU6050_driver.i2c, MPU6050_driver.Address, 200,10);
        if(hs==HAL_OK)
        {
            MPU6050_setClockSource(clkSrc);
            MPU6050_setAccelRange(accelRange); // *
            MPU6050_setGyroRange(gyroRange); // *
            MPU6050_setSamplingRate(); //1kHZ default
            MPU6050_setDLPF(); //
            MPU6050_enableDataReadyInt(); //
            MPU6050_setHWInt(); //
            //HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_0);
        }
    }
    return hs;
}

/*****************************************************/
/*            MPU6050 complementary functions       */
/****************************************************/
/*
int16_t MPU6050_calibrate(MPU6050_t *DataStruct)
{
    int i;
    for(i = 0; i<100; i++)
    {
        calibVals[0] += DataStruct->Accelerometer_X_H <<8 | DataStruct->Accelerometer_X_L;
        calibVals[1] += DataStruct->Accelerometer_Y_H <<8 | DataStruct->Accelerometer_Y_L;
        calibVals[2] += DataStruct->Accelerometer_Z_H <<8 | DataStruct->Accelerometer_Z_L;
    }
    calibVals[0] = calibVals[0]/100;


}
*/
int16_t MPU6050_movingAverageFilter(MPU6050_t *DataStruct)
{
    int count0 = 0;
    int16_t xAcc_f;
    int16_t xSample;
    do
    {
        xSample = DataStruct->Accelerometer_X_H >>8 | DataStruct->Accelerometer_X_L;
        xAcc_f += xSample;
    }
    while(count0!= 64);
    xAcc_f = xAcc_f/64;
    return xAcc_f;
}

MPU6050_Flag MPU6050_getMovingState(MPU6050_t *DataStruct)
{
    int count0 = 0;
    int16_t xAccSample;
    xAccSample = DataStruct->Accelerometer_X_H >>8 | DataStruct->Accelerometer_X_L;
    if(xAccSample < 255 || xAccSample > -255)
    {
        count0 ++;
    }
    else
    {
        return MPU6050_IS_MOVING;
    }
    if(count0>=25)
    {
        return MPU6050_IS_NOT_MOVING;
    }
    return 0;

}

/*******************************************/
/*            MPU6050 configuration       */
/*******************************************/

MPU6050_Result_t setLowPower(uint8_t clkSrc)
{
    I2Cdata[0] = MPU6050_PWR_MGMT_1;
    I2Cdata[1] = clkSrc;
    HAL_I2C_Master_Transmit(MPU6050_driver.i2c, MPU6050_ADDRESS_AD0_LOW, I2Cdata, 2, 10);
    //set wakeup freq
    I2Cdata[0] = MPU6050_PWR_MGMT_2;
    I2Cdata[1] = 0xC7; //c0
    HAL_I2C_Master_Transmit(MPU6050_driver.i2c, MPU6050_ADDRESS_AD0_LOW, I2Cdata, 2, 10);
    return MPU6050_Result_Ok;
}

MPU6050_Result_t MPU650_setIntLatched(int en)
{
    if(en==1)
    {
        I2Cdata[0] = MPU6050_INT_PIN_CFG;
        I2Cdata[1] = 0b11010000;
        HAL_I2C_Master_Transmit(MPU6050_driver.i2c, MPU6050_ADDRESS_AD0_LOW, I2Cdata, 2, 10);
        return MPU6050_Result_Ok;
    }
    else
    {
        I2Cdata[0] = MPU6050_INT_PIN_CFG;
        I2Cdata[1] = 0x00;
        HAL_I2C_Master_Transmit(MPU6050_driver.i2c, MPU6050_ADDRESS_AD0_LOW, I2Cdata, 2, 10);
        return MPU6050_Result_Ok;
    }
}

MPU6050_Result_t MPU6050_setIntEnabled(int en)
{
    if(en==1)
    {
        I2Cdata[0] = MPU6050_INT_ENABLE;
        I2Cdata[1] = 0b01000001;
        HAL_I2C_Master_Transmit(MPU6050_driver.i2c, MPU6050_ADDRESS_AD0_LOW, I2Cdata, 2, 10);
        return MPU6050_Result_Ok;
    }
    else
    {
        I2Cdata[0] = MPU6050_INT_ENABLE;
        I2Cdata[1] = 0x0;
        HAL_I2C_Master_Transmit(MPU6050_driver.i2c, MPU6050_ADDRESS_AD0_LOW, I2Cdata, 2, 10);
        return MPU6050_Result_Ok;
    }
}

MPU6050_Result_t MPU6050_setDLPF()
{
    I2Cdata[0] = MPU6050_CONFIG;
    I2Cdata[1] = 0x0;
    HAL_I2C_Master_Transmit(MPU6050_driver.i2c, MPU6050_ADDRESS_AD0_LOW, I2Cdata, 2, 10);
    return MPU6050_Result_Ok;
}

MPU6050_Result_t MPU6050_enableMotionDetectionInt()
{
    I2Cdata[0] = MPU6050_INT_ENABLE;
    I2Cdata[1] = TM_MPU6050_MOT_INT_EN;
    HAL_I2C_Master_Transmit(MPU6050_driver.i2c, MPU6050_ADDRESS_AD0_LOW, I2Cdata, 2, 10);
    return MPU6050_Result_Ok;
}

MPU6050_Result_t MPU6050_enableDataReadyInt()
{
    I2Cdata[0] = MPU6050_INT_ENABLE;
    I2Cdata[1] = TM_MPU6050_DTA_RDY_MOTION_INT;
    HAL_I2C_Master_Transmit(MPU6050_driver.i2c, MPU6050_ADDRESS_AD0_LOW, I2Cdata, 2, 10);
    return MPU6050_Result_Ok;
}

MPU6050_Result_t MPU6050_setHWInt()
{
    I2Cdata[0] = MPU6050_INT_PIN_CFG;
    I2Cdata[1] = TM_MPU6050_INT_ACTIVE_LOW;
    HAL_I2C_Master_Transmit(MPU6050_driver.i2c, MPU6050_ADDRESS_AD0_LOW, I2Cdata, 2, 10);
    return MPU6050_Result_Ok;
}

MPU6050_Result_t MPU650_setMotionTreshold()
{
    I2Cdata[0] = MPU6050_MOT_THR;
    I2Cdata[1] = 0xFF; // 255 treshold
    HAL_I2C_Master_Transmit(MPU6050_driver.i2c, MPU6050_ADDRESS_AD0_LOW, I2Cdata, 2, 10);
    return MPU6050_Result_Ok;
}

uint8_t MPU650_getMotionIntStatus()
{
    I2Cdata[0] = MPU6050_INT_STATUS;
    HAL_I2C_Master_Transmit(MPU6050_driver.i2c, MPU6050_ADDRESS_AD0_LOW, I2Cdata, 1, 10);
    HAL_I2C_Master_Receive(MPU6050_driver.i2c, MPU6050_ADDRESS_AD0_LOW, I2Cdata, 1, 10);
    motionIntStatus = I2Cdata[0];
    return motionIntStatus;
}

MPU6050_Result_t MPU6050_setAccelRange(uint8_t range)
{
    I2Cdata[0] = MPU6050_ACCEL_CONFIG;
    I2Cdata[1] = range;
    HAL_I2C_Master_Transmit(MPU6050_driver.i2c, MPU6050_ADDRESS_AD0_LOW, I2Cdata, 2, 10);
    return MPU6050_Result_Ok;
}

MPU6050_Result_t MPU6050_setGyroRange(uint8_t range)
{
    I2Cdata[0] = MPU6050_GYRO_CONFIG;
    I2Cdata[1] = range;
    HAL_I2C_Master_Transmit(MPU6050_driver.i2c, MPU6050_ADDRESS_AD0_LOW, I2Cdata, 2, 10);
    return MPU6050_Result_Ok;
}

MPU6050_Result_t MPU6050_setClockSource(uint8_t clkSrc)
{
    I2Cdata[0] = MPU6050_PWR_MGMT_1;
    I2Cdata[1] = TM_MPU6050_PLL_XGYRO_CLOCK; //PLL with X axis gyro reference + wakeup
    HAL_I2C_Master_Transmit(MPU6050_driver.i2c, MPU6050_ADDRESS_AD0_LOW, I2Cdata, 2, 10);
    return MPU6050_Result_Ok;
}

MPU6050_Result_t MPU6050_setSamplingRate()
{
    I2Cdata[0] = MPU6050_SMPLRT_DIV;
    I2Cdata[1] = TM_MPU6050_DataRate_1KHz; //1kHz sampling rate
    HAL_I2C_Master_Transmit(MPU6050_driver.i2c, MPU6050_ADDRESS_AD0_LOW, I2Cdata, 2, 10);
    return MPU6050_Result_Ok;
}



/***********************************************/
/*            MPU6050 data gathering         */
/***********************************************/

/**
* @brief Loads raw accelerometer and gyroscope values + temperature.
* byte: 0, 1: x-axis rotation
* byte: 2, 3: y-axis rotation
* byte: 4, 5: z-axis rotation
* byte: 6, 7: temperature
* byte: 8, 9: x-axis acceleration
* byte: 10, 11: y-axis acceleration
* byte: 12, 13: z-axis acceleration
* @param DataStruct : pointer to MPU6050_t struct
* @return MPU6050_Result_t
*/
MPU6050_Result_t MPU6050_getMotion6(MPU6050_t *DataStruct)
{
    data[0] = MPU6050_ACCEL_XOUT_H;
    //Sending register address
    HAL_I2C_Master_Transmit(MPU6050_driver.i2c, MPU6050_ADDRESS_AD0_LOW, data, 1, 10);
    //Reading response of size 14
    HAL_I2C_Master_Receive(MPU6050_driver.i2c, MPU6050_ADDRESS_AD0_LOW, readings, 14, 10);

    DataStruct->Accelerometer_X_H = (readings[0]);
    DataStruct->Accelerometer_X_L = (readings[1]);
    DataStruct->Accelerometer_Y_H = (readings[2]);
    DataStruct->Accelerometer_Y_L = (readings[3]);
    DataStruct->Accelerometer_Z_H = (readings[4]);
    DataStruct->Accelerometer_Z_L = (readings[5]);
    DataStruct->Temperature_H = (readings[6]);
    DataStruct->Temperature_L = (readings[7]);
    DataStruct->Gyroscope_X_H = (readings[8]);
    DataStruct->Gyroscope_X_L = (readings[9]);
    DataStruct->Gyroscope_Y_H = (readings[10]);
    DataStruct->Gyroscope_Y_L = (readings[11]);
    DataStruct->Gyroscope_Z_H = (readings[12]);
    DataStruct->Gyroscope_Z_L = (readings[13]);

    return MPU6050_Result_Ok;
}

/**
* @brief Loads raw accelerometer values.
* @param DataStruct : pointer to MPU6050_t struct
* byte: 0, 1: x-axis acceleration
* byte: 2, 3: y-axis acceleration
* byte: 4, 5: z-axis acceleration
* @return MPU6050_Result_t
*/
MPU6050_Result_t MPU6050_getAcceleration(MPU6050_t *DataStruct)
{
    data[0] = MPU6050_GYRO_XOUT_H;
    // Sending MPU6050_ACCEL_XOUT_H register address
    HAL_I2C_Master_Transmit(MPU6050_driver.i2c, MPU6050_ADDRESS_AD0_LOW, data, 1, 10);
    // Waiting for response from MPU6050_ACCEL_XOUT_H address
    HAL_I2C_Master_Receive(MPU6050_driver.i2c, MPU6050_ADDRESS_AD0_LOW, readings, 6, 10);

    DataStruct->Accelerometer_X_H = (readings[0]);
    DataStruct->Accelerometer_X_L = (readings[1]);
    DataStruct->Accelerometer_Y_H = (readings[2]);
    DataStruct->Accelerometer_Y_L = (readings[3]);
    DataStruct->Accelerometer_Z_H = (readings[4]);
    DataStruct->Accelerometer_Z_L = (readings[5]);

    return MPU6050_Result_Ok;
}


/**
* @brief Loads raw temperature values.
* @param DataStruct : pointer to MPU6050_t struct
* byte 0, 1: temperature
* @return MPU6050_Result_t
*/
MPU6050_Result_t MPU6050_getTemperature(MPU6050_t *DataStruct)
{
    data[0] = MPU6050_TEMP_OUT_H;
    // Sending MPU6050_TEMP_OUT_H register address
    HAL_I2C_Master_Transmit(MPU6050_driver.i2c, MPU6050_ADDRESS_AD0_LOW, data, 1, 10);
    // Waiting for response from MPU6050_TEMP_OUT_H address
    HAL_I2C_Master_Receive(MPU6050_driver.i2c, MPU6050_ADDRESS_AD0_LOW, readings, 2, 10);

    DataStruct->Temperature_H = (readings[0]);
    DataStruct->Temperature_L = (readings[1]);

    return MPU6050_Result_Ok;
}

/**
* @brief Loads raw gyroscope values.
* @param DataStruct : pointer to MPU6050_t struct
* byte: 0, 1: x-axis rotation
* byte: 2, 3: y-axis rotation
* byte: 4, 5: z-axis rotation
* @return MPU6050_Result_t
*/
MPU6050_Result_t MPU6050_getRotation(MPU6050_t *DataStruct)
{
    data[0] = MPU6050_ACCEL_XOUT_H;
    // Sending MPU6050_GYRO_XOUT_H register address
    HAL_I2C_Master_Transmit(MPU6050_driver.i2c, MPU6050_ADDRESS_AD0_LOW, data, 1, 10);
    // Waiting for response from MPU6050_GYRO_XOUT_H address
    HAL_I2C_Master_Receive(MPU6050_driver.i2c, MPU6050_ADDRESS_AD0_LOW, readings, 6, 10);

    DataStruct->Gyroscope_X_H = (readings[0]);
    DataStruct->Gyroscope_X_L = (readings[1]);
    DataStruct->Gyroscope_Y_H = (readings[2]);
    DataStruct->Gyroscope_Y_L = (readings[3]);
    DataStruct->Gyroscope_Z_H = (readings[4]);
    DataStruct->Gyroscope_Z_L = (readings[6]);
    return MPU6050_Result_Ok;
}


