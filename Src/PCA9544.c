// PCA9544 library
//-----------------------
// Author: Pavol Dudak
//
// October 2015
//      first version
//
// MIT license
// For STM32F030
//
// Documentation:
//          PCA9544 4-channel I2C bus multiplexer with interrupt logic - Product Data sheet (Rev. 5 23. April 2014)

#include "stm32f0xx_hal.h"
#include "PCA9544.h"

extern I2C_HandleTypeDef hi2c1;
uint8_t recData[1];

PCA9544_Result_t PCA9544_Init(I2C_HandleTypeDef *hi2c)
{
    hi2c1 = *hi2c;
    return PCA9544_Result_Ok;
}

PCA9544_Result_t PCA9544_enableChannel(uint8_t ch)
{
    uint8_t controlRegister[1] = {0x04};
    controlRegister[0] |= ch;

    if (ch == 0xFF)
    {
        HAL_I2C_Master_Transmit(&hi2c1, muxAddr, controlRegister, 1, 10);
    }
    else
    {
        HAL_I2C_Master_Transmit(&hi2c1, muxAddr, controlRegister, 1, 10);
    }
    return PCA9544_Result_Ok;
}

PCA9544_Result_t PCA9544_enableCh1()
{
    uint8_t controlRegister[2] = {0xFC};
    HAL_I2C_Master_Transmit(&hi2c1, muxAddr, controlRegister, 1, 10);

    uint8_t odp[1] = {0xEB};
    HAL_I2C_Master_Transmit(&hi2c1, odp[0] , odp, 1, 10);
    return PCA9544_Result_Ok;
}
