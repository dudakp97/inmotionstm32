/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * COPYRIGHT(c) 2016 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"

/* USER CODE BEGIN Includes */
#include "MPU6050.h"
#include "btHandler.h"
#include "PCA9544.h"
#include "main.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

UART_HandleTypeDef huart1;
DMA_HandleTypeDef hdma_usart1_tx;

/* USER CODE BEGIN PV */
#if MPU6050_COUNT == 1
MPU6050_t sens0MPUstruct;
#endif // MPU6050_COUNT
#if MPU6050_COUNT == 2
MPU6050_t sens0MPUstruct;
MPU6050_t sens1MPUstruct;
#endif // MPU6050_COUNT


uint8_t dataBuffer[27+MPU6050_COUNT*14];
uint8_t dataStartBits[2] = {0x25, 0x72};
uint32_t t_now;
uint32_t t_last = 0;

uint32_t deltaTime;

int16_t val_rawX;
int16_t val_step_1_X;
int16_t val_step_2_X;
int32_t val_inte_first_X;
int32_t val_inte_second_X;

int16_t val_rawY;
int16_t val_step_1_Y;
int16_t val_step_2_Y;
int32_t val_inte_first_Y;
int32_t val_inte_second_Y;

int16_t val_rawZ;
int16_t val_step_1_Z;
int16_t val_step_2_Z;
int32_t val_inte_first_Z;
int32_t val_inte_second_Z;

__IO uint8_t flag_exti;
__IO uint8_t flag_init;

int8_t val_send_x_first[4];
int8_t val_send_x_second[4];
uint8_t UART_rec_status = 0;
uint8_t rd[5];
uint16_t accPreslacer = ACC_REAL_VAL_PRESCALER_2G;
uint16_t gyroPreslacer;


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_I2C1_Init(void);
static void MX_USART1_UART_Init(void);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */


void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
    flag_init = 1;
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {
    if(GPIO_Pin == IMU_INT_Pin){
        flag_exti = 0;
    }
}
/* USER CODE END 0 */

int main(void) {

    /* USER CODE BEGIN 1 */

    /* USER CODE END 1 */

    /* MCU Configuration----------------------------------------------------------*/

    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    HAL_Init();

    /* Configure the system clock */
    SystemClock_Config();

    /* Initialize all configured peripherals */
    MX_GPIO_Init();
    MX_DMA_Init();
    MX_I2C1_Init();
    MX_USART1_UART_Init();

    /* USER CODE BEGIN 2 */
    HAL_NVIC_DisableIRQ(EXTI4_15_IRQn);
#if MPU6050_COUNT == 1
    //PCA9544_enableChannel(1);
    MPU6050_Init(&hi2c1, &sens0MPUstruct, MPU6050_ADDRESS_AD0_LOW);
#endif // MPU6050_COUNT
#if MPU6050_COUNT == 2
    PCA9544_enableChannel(1);
    MPU6050_Init(&hi2c1, &sens0MPUstruct, MPU6050_ADDRESS_AD0_LOW);
    PCA9544_enableChannel(2);
    MPU6050_Init(&hi2c1, &sens1MPUstruct, MPU6050_ADDRESS_AD0_LOW);
#endif // MPU6050_COUNT


    accPreslacer = ACC_REAL_VAL_PRESCALER_2G;
    //externa koonfiguracia senzorov
    /*
        HAL_UART_Receive_IT(&huart1, rd, 2);
        while(flag_init == 0)
        {
            ;
        }
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, 0);

        if(rd[0]=='C')
        {
            //MPU6050_Init_ext(&hi2c1, &sens0MPUstruct, MPU6050_DEFAULT_ADDRESS, rd[1], rd[2], rd[3]);
            switch(rd[2])
            {
            case 0x0:
                accPreslacer = ACC_REAL_VAL_PRESCALER_2G;
                break;
            case 0x8:
                accPreslacer = ACC_REAL_VAL_PRESCALER_4G;
                break;
            case 0x10:
                accPreslacer = ACC_REAL_VAL_PRESCALER_8G;
                break;
            case 0x18:
                accPreslacer = ACC_REAL_VAL_PRESCALER_16G;
                break;
            }
            switch (rd[3])
            {
            case 0x0:
                gyroPreslacer = GYRO_REAL_VAL_PRESCALER_250;
                break;
            case 0x8:
                gyroPreslacer = GYRO_REAL_VAL_PRESCALER_500;
                break;
            case 0x10:
                gyroPreslacer = GYRO_REAL_VAL_PRESCALER_1000;
                break;
            case 0x18:
                gyroPreslacer = GYRO_REAL_VAL_PRESCALER_2000;
                break;
            }
            flag_init = 1;

        }
    */


    HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);

    /* USER CODE END 2 */

    /* Infinite loop */
    /* USER CODE BEGIN WHILE */
    uint8_t index;
    while (1) {

        t_now = HAL_GetTick();
        deltaTime = (t_now - t_last);
        t_last=t_now;

        LED1_ON;
        flag_exti = 1;
        while(flag_exti);
        LED1_OFF;

        HAL_NVIC_DisableIRQ(EXTI4_15_IRQn);
        //prisprava dat na odoslanie
#if MPU6050_COUNT == 1
        MPU6050_getMotion6(&sens0MPUstruct);
#endif // MPU6050_COUNT
#if MPU6050_COUNT == 2
        PCA9544_enableChannel(0);
        MPU6050_getMotion6(&sens0MPUstruct);
        PCA9544_enableChannel(1);
        MPU6050_getMotion6(&sens1MPUstruct);
#endif // MPU6050_COUNT
        index=0;
        dataBuffer[index++] = dataStartBits[0];
        dataBuffer[index++] = dataStartBits[1];
        dataBuffer[index++] = sens0MPUstruct.acc_range;

        dataBuffer[index++] = sens0MPUstruct.Accelerometer_X_H;
        dataBuffer[index++] = sens0MPUstruct.Accelerometer_X_L;
        dataBuffer[index++] = sens0MPUstruct.Accelerometer_Y_H;
        dataBuffer[index++] = sens0MPUstruct.Accelerometer_Y_L;
        dataBuffer[index++] = sens0MPUstruct.Accelerometer_Z_H;
        dataBuffer[index++] = sens0MPUstruct.Accelerometer_Z_L;
        /*
        dataBuffer[index++] = sens0MPUstruct.Accelerometer_Z_H;
        dataBuffer[index++] = sens0MPUstruct.Accelerometer_Z_L;
        dataBuffer[index++] = sens0MPUstruct.Temperature_H;
        dataBuffer[index++] = sens0MPUstruct.Temperature_L;
        dataBuffer[index++] = sens0MPUstruct.Gyroscope_X_H;
        dataBuffer[index++] = sens0MPUstruct.Gyroscope_X_L;
        dataBuffer[index++] = sens0MPUstruct.Gyroscope_Y_H;
        dataBuffer[index++] = sens0MPUstruct.Gyroscope_Y_L;
        dataBuffer[index++] = sens0MPUstruct.Gyroscope_Z_H;
        dataBuffer[index++] = sens0MPUstruct.Gyroscope_Z_L;
        */

#if MPU6050_COUNT == 2
#error TERAZ PRACUJEM LEN S JEDNYM SENZOROM
        dataBuffer[16] = sens1MPUstruct.Accelerometer_X_H;
        dataBuffer[17] = sens1MPUstruct.Accelerometer_X_L;
        dataBuffer[18] = sens1MPUstruct.Accelerometer_Y_H;
        dataBuffer[19] = sens1MPUstruct.Accelerometer_Y_L;
        dataBuffer[20] = sens1MPUstruct.Accelerometer_Z_H;
        dataBuffer[21] = sens1MPUstruct.Accelerometer_Z_L;
        dataBuffer[22] = sens1MPUstruct.Temperature_H;
        dataBuffer[23] = sens1MPUstruct.Temperature_L;
        dataBuffer[24] = sens1MPUstruct.Gyroscope_X_H;
        dataBuffer[25] = sens1MPUstruct.Gyroscope_X_L;
        dataBuffer[26] = sens1MPUstruct.Gyroscope_Y_H;
        dataBuffer[27] = sens1MPUstruct.Gyroscope_Y_L;
        dataBuffer[28] = sens1MPUstruct.Gyroscope_Z_H;
        dataBuffer[29] = sens1MPUstruct.Gyroscope_Z_L;
        index = 29;
#endif // MPU6050_COUNT
/*
        //integracia x-ovych hodnot
        val_rawX = (sens0MPUstruct.Accelerometer_X_H <<8) | (sens0MPUstruct.Accelerometer_X_L);
        val_rawX = accPreslacer - val_rawX;
        val_step_1_X = (val_rawX * deltaTime);
        val_inte_first_X += val_step_1_X;
        val_step_2_X = (val_inte_first_X * deltaTime);
        val_inte_second_X += val_step_2_X;

        //integracia y-ovych hodnot
        val_rawY = (sens0MPUstruct.Accelerometer_Y_H <<8) | sens0MPUstruct.Accelerometer_Y_L;
        val_rawY = accPreslacer - val_rawY;
        val_step_1_Y = (val_rawY * deltaTime);
        val_inte_first_Y += val_step_1_Y;
        val_step_2_Y = (val_inte_first_Y * deltaTime);
        val_inte_second_Y += val_step_2_Y;

        //integracia z-ovych hodnot
        val_rawZ = (sens0MPUstruct.Accelerometer_Z_H <<8) | sens0MPUstruct.Accelerometer_Z_L;
        val_rawZ = accPreslacer - val_rawZ ;
        val_step_1_Z = (val_rawZ * deltaTime);
        val_inte_first_Z += val_step_1_Z;
        val_step_2_Z = (val_inte_first_Z * deltaTime);
        val_inte_second_Z += val_step_2_Z;

        //nacitanie x-ovych prepocitanych hodnot do buffera
        dataBuffer[index++] = val_inte_first_X >> 24;
        dataBuffer[index++] = (val_inte_first_X >> 16) & 0xFF;
        dataBuffer[index++] = (val_inte_first_X >> 8) & 0xFF;
        dataBuffer[index++] = val_inte_first_X & 0xFF;
        dataBuffer[index++] = val_inte_second_X >> 24;
        dataBuffer[index++] = (val_inte_second_X >> 16) & 0xFF;
        dataBuffer[index++] = (val_inte_second_X >> 8) & 0xFF;
        dataBuffer[index++] = val_inte_second_X & 0xFF;
*/
/*
        //nacitanie y-ovych prepocitanych hodnot do buffera
        dataBuffer[index++] = val_inte_first_Y >> 24;
        dataBuffer[index++] = (val_inte_first_Y >> 16) & 0xFF;
        dataBuffer[index++] = (val_inte_first_Y >> 8) & 0xFF;
        dataBuffer[index++] = val_inte_first_Y & 0xFF;
        dataBuffer[index++] = val_inte_second_Y >> 24;
        dataBuffer[index++] = (val_inte_second_Y >> 16) & 0xFF;
        dataBuffer[index++] = (val_inte_second_Y >> 8) & 0xFF;
        dataBuffer[index++] = val_inte_second_Y & 0xFF;


        //nacitaniezx-ovych prepocitanych hodnot do buffera
        dataBuffer[index++] = val_inte_first_Z >> 24;
        dataBuffer[index++] = (val_inte_first_Z >> 16) & 0xFF;
        dataBuffer[index++] = (val_inte_first_Z >> 8) & 0xFF;
        dataBuffer[index++] = val_inte_first_Z & 0xFF;
        dataBuffer[index++] = val_inte_second_Z >> 24;
        dataBuffer[index++] = (val_inte_second_Z >> 16) & 0xFF;
        dataBuffer[index++] = (val_inte_second_Z >> 8) & 0xFF;
        dataBuffer[index++] = val_inte_second_Z & 0xFF;                 //53

*/
        //dataBuffer[index++] = index;
        HAL_UART_Transmit(&huart1, dataBuffer, index, 500);
        //HAL_UART_Transmit_DMA(&huart1, dataBuffer, index);
        HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);

    }

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

    /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void) {

    RCC_OscInitTypeDef RCC_OscInitStruct;
    RCC_ClkInitTypeDef RCC_ClkInitStruct;
    RCC_PeriphCLKInitTypeDef PeriphClkInit;

    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
    RCC_OscInitStruct.HSIState = RCC_HSI_ON;
    RCC_OscInitStruct.HSICalibrationValue = 16;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
    RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL12;
    RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
        Error_Handler();
    }

    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                                  |RCC_CLOCKTYPE_PCLK1;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK) {
        Error_Handler();
    }

    PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_I2C1;
    PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
    PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
    if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK) {
        Error_Handler();
    }

    HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

    /* SysTick_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* I2C1 init function */
static void MX_I2C1_Init(void) {

    hi2c1.Instance = I2C1;
    hi2c1.Init.Timing = 0x0000020B;
    hi2c1.Init.OwnAddress1 = 0;
    hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
    hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
    hi2c1.Init.OwnAddress2 = 0;
    hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
    hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
    hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
    if (HAL_I2C_Init(&hi2c1) != HAL_OK) {
        Error_Handler();
    }

    /**Configure Analogue filter
    */
    if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK) {
        Error_Handler();
    }

}

/* USART1 init function */
static void MX_USART1_UART_Init(void) {

    huart1.Instance = USART1;
    huart1.Init.BaudRate = 115200;
    huart1.Init.WordLength = UART_WORDLENGTH_8B;
    huart1.Init.StopBits = UART_STOPBITS_1;
    huart1.Init.Parity = UART_PARITY_NONE;
    huart1.Init.Mode = UART_MODE_TX_RX;
    huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    huart1.Init.OverSampling = UART_OVERSAMPLING_16;
    huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
    huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
    if (HAL_UART_Init(&huart1) != HAL_OK) {
        Error_Handler();
    }

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) {
    /* DMA controller clock enable */
    __HAL_RCC_DMA1_CLK_ENABLE();

    /* DMA interrupt init */
    /* DMA1_Channel2_3_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(DMA1_Channel2_3_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(DMA1_Channel2_3_IRQn);

}

/** Configure pins as
        * Analog
        * Input
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void) {

    GPIO_InitTypeDef GPIO_InitStruct;

    /* GPIO Ports Clock Enable */
    __HAL_RCC_GPIOA_CLK_ENABLE();

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(GPIOA, LED1_Pin|BT_KEY_Pin, GPIO_PIN_RESET);

    /*Configure GPIO pins : LED1_Pin BT_KEY_Pin */
    GPIO_InitStruct.Pin = LED1_Pin|BT_KEY_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /*Configure GPIO pin : IMU_INT_Pin */
    GPIO_InitStruct.Pin = IMU_INT_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(IMU_INT_GPIO_Port, &GPIO_InitStruct);

    /* EXTI interrupt init*/
    HAL_NVIC_SetPriority(EXTI4_15_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);

}

/* USER CODE BEGIN 4 */





void HAL_SYSTICK_Callback(void) {
    //HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_0);
}
/*
    void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
    {

    }
*/
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void) {
    /* USER CODE BEGIN Error_Handler */
    /* User can add his own implementation to report the HAL error return state */
    while(1) {
    }
    /* USER CODE END Error_Handler */
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line) {
    /* USER CODE BEGIN 6 */
    /* User can add his own implementation to report the file name and line number,
      ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
    /* USER CODE END 6 */

}

#endif

/**
  * @}
  */

/**
  * @}
*/

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
