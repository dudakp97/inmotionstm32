#ifndef MAIN_H
#define MAIN_H
#include "stm32f0xx_hal.h"

#define MPU6050_COUNT   1

#define LED1_ON		(LED1_GPIO_Port->BSRR=LED1_Pin)
#define LED1_OFF	(LED1_GPIO_Port->BRR =LED1_Pin)
#define LED1_TOGGLE	(LED1_GPIO_Port->ODR^=LED1_Pin)

#endif
