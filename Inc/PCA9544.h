// PCA9544 library
//-----------------------
// Author: Pavol Dudak
//
// October 2015
//      first version
//
// MIT license
// For STM32F030
//
// Documentation:
//          PCA9544 4-channel I2C bus multiplexer with interrupt logic - Product Data sheet (Rev. 5 23. April 2014)
#ifndef PCA9544_H
#define PCA9544_H
#include "stdint.h"

#define muxAddr 0x70<<1

typedef enum {
	PCA9544_Result_Ok = 0x00,          // Everything OK
} PCA9544_Result_t;

PCA9544_Result_t PCA9544_enableChannel(uint8_t ch);
PCA9544_Result_t PCA9544_Init(I2C_HandleTypeDef *hi2c);
PCA9544_Result_t PCA9544_enableCh1();

#endif
