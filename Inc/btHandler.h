#ifndef BT_HANDLER
#define BT_HANDLER

#include "stm32f0xx_hal.h"

/**
 * @brief  Main HC-05 structure
 */

typedef enum {
	HC05_Result_Ok = 0x00,          // Everything OK
	HC05_Result_DeviceNotConnected, // There is no device with valid slave address
} HC05_Result_t;

typedef enum bt_cmd{
    BT_ENTER_AT = 0x00,
    BT_CMD_INC,
    BT_EXIT_AT,
    BT_UNKNOWN_CMD
}BT_CMD_t;

typedef struct {
    uint8_t HC05_State;
} HC05_t;

HC05_Result_t HC05_initBt(UART_HandleTypeDef *huart);
HC05_Result_t HC05_getState(HC05_t *DataStruct);
HC05_Result_t HC05_setAT();
HC05_Result_t HC05_checkCommand();
HC05_Result_t HC05_receiveCMD();
HC05_Result_t HC05_writeCmd();

#endif // BT_HANDLER
