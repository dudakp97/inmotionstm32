#ifndef MPU6050_H
#define MPU6050_H

#include "stm32f0xx_hal.h"

#define MPU6050_ADDRESS_AD0_LOW     0x68<<1 // address pin low (GND), default for InvenSense evaluation board
#define MPU6050_ADDRESS_AD0_HIGH    0x69<<1 // address pin high (VCC)
#define MPU6050_DEFAULT_ADDRESS     MPU6050_ADDRESS_AD0_LOW

//#define MPU9150_SELF_TEST_X        0x0D   // R/W
#define MPU6050_AUX_VDDIO          0x01   // R/W
#define MPU6050_SMPLRT_DIV         0x19   // R/W
#define MPU6050_CONFIG             0x1A   // R/W
#define MPU6050_GYRO_CONFIG        0x1B   // R/W
#define MPU6050_ACCEL_CONFIG       0x1C   // R/W
#define MPU6050_FF_THR             0x1D   // R/W
#define MPU6050_FF_DUR             0x1E   // R/W
#define MPU6050_MOT_THR            0x1F   // R/W
#define MPU6050_MOT_DUR            0x20   // R/W
#define MPU6050_ZRMOT_THR          0x21   // R/W
#define MPU6050_ZRMOT_DUR          0x22   // R/W
#define MPU6050_FIFO_EN            0x23   // R/W
#define MPU6050_I2C_MST_CTRL       0x24   // R/W
#define MPU6050_I2C_SLV0_ADDR      0x25   // R/W
#define MPU6050_I2C_SLV0_REG       0x26   // R/W
#define MPU6050_I2C_SLV0_CTRL      0x27   // R/W
#define MPU6050_I2C_SLV1_ADDR      0x28   // R/W
#define MPU6050_I2C_SLV1_REG       0x29   // R/W
#define MPU6050_I2C_SLV1_CTRL      0x2A   // R/W
#define MPU6050_I2C_SLV2_ADDR      0x2B   // R/W
#define MPU6050_I2C_SLV2_REG       0x2C   // R/W
#define MPU6050_I2C_SLV2_CTRL      0x2D   // R/W
#define MPU6050_I2C_SLV3_ADDR      0x2E   // R/W
#define MPU6050_I2C_SLV3_REG       0x2F   // R/W
#define MPU6050_I2C_SLV3_CTRL      0x30   // R/W
#define MPU6050_I2C_SLV4_ADDR      0x31   // R/W
#define MPU6050_I2C_SLV4_REG       0x32   // R/W
#define MPU6050_I2C_SLV4_DO        0x33   // R/W
#define MPU6050_I2C_SLV4_CTRL      0x34   // R/W
#define MPU6050_I2C_SLV4_DI        0x35   // R
#define MPU6050_I2C_MST_STATUS     0x36   // R
#define MPU6050_INT_PIN_CFG        0x37   // R/W
#define MPU6050_INT_ENABLE         0x38   // R/W
#define MPU6050_INT_STATUS         0x3A   // R
#define MPU6050_ACCEL_XOUT_H       0x3B   // R
#define MPU6050_ACCEL_XOUT_L       0x3C   // R
#define MPU6050_ACCEL_YOUT_H       0x3D   // R
#define MPU6050_ACCEL_YOUT_L       0x3E   // R
#define MPU6050_ACCEL_ZOUT_H       0x3F   // R
#define MPU6050_ACCEL_ZOUT_L       0x40   // R
#define MPU6050_TEMP_OUT_H         0x41   // R
#define MPU6050_TEMP_OUT_L         0x42   // R
#define MPU6050_GYRO_XOUT_H        0x43   // R
#define MPU6050_GYRO_XOUT_L        0x44   // R
#define MPU6050_GYRO_YOUT_H        0x45   // R
#define MPU6050_GYRO_YOUT_L        0x46   // R
#define MPU6050_GYRO_ZOUT_H        0x47   // R
#define MPU6050_GYRO_ZOUT_L        0x48   // R
#define MPU6050_EXT_SENS_DATA_00   0x49   // R
#define MPU6050_EXT_SENS_DATA_01   0x4A   // R
#define MPU6050_EXT_SENS_DATA_02   0x4B   // R
#define MPU6050_EXT_SENS_DATA_03   0x4C   // R
#define MPU6050_EXT_SENS_DATA_04   0x4D   // R
#define MPU6050_EXT_SENS_DATA_05   0x4E   // R
#define MPU6050_EXT_SENS_DATA_06   0x4F   // R
#define MPU6050_EXT_SENS_DATA_07   0x50   // R
#define MPU6050_EXT_SENS_DATA_08   0x51   // R
#define MPU6050_EXT_SENS_DATA_09   0x52   // R
#define MPU6050_EXT_SENS_DATA_10   0x53   // R
#define MPU6050_EXT_SENS_DATA_11   0x54   // R
#define MPU6050_EXT_SENS_DATA_12   0x55   // R
#define MPU6050_EXT_SENS_DATA_13   0x56   // R
#define MPU6050_EXT_SENS_DATA_14   0x57   // R
#define MPU6050_EXT_SENS_DATA_15   0x58   // R
#define MPU6050_EXT_SENS_DATA_16   0x59   // R
#define MPU6050_EXT_SENS_DATA_17   0x5A   // R
#define MPU6050_EXT_SENS_DATA_18   0x5B   // R
#define MPU6050_EXT_SENS_DATA_19   0x5C   // R
#define MPU6050_EXT_SENS_DATA_20   0x5D   // R
#define MPU6050_EXT_SENS_DATA_21   0x5E   // R
#define MPU6050_EXT_SENS_DATA_22   0x5F   // R
#define MPU6050_EXT_SENS_DATA_23   0x60   // R
#define MPU6050_MOT_DETECT_STATUS  0x61   // R
#define MPU6050_I2C_SLV0_DO        0x63   // R/W
#define MPU6050_I2C_SLV1_DO        0x64   // R/W
#define MPU6050_I2C_SLV2_DO        0x65   // R/W
#define MPU6050_I2C_SLV3_DO        0x66   // R/W
#define MPU6050_I2C_MST_DELAY_CTRL 0x67   // R/W
#define MPU6050_SIGNAL_PATH_RESET  0x68   // R/W
#define MPU6050_MOT_DETECT_CTRL    0x69   // R/W
#define MPU6050_USER_CTRL          0x6A   // R/W
#define MPU6050_PWR_MGMT_1         0x6B   // R/W
#define MPU6050_PWR_MGMT_2         0x6C   // R/W
#define MPU6050_FIFO_COUNTH        0x72   // R/W
#define MPU6050_FIFO_COUNTL        0x73   // R/W
#define MPU6050_FIFO_R_W           0x74   // R/W
#define MPU6050_WHO_AM_I           0x75   // R

#define MPU6050_CFG_EXT_SYNC_SET_BIT    5
#define MPU6050_CFG_EXT_SYNC_SET_LENGTH 3
#define MPU6050_CFG_DLPF_CFG_BIT    2
#define MPU6050_CFG_DLPF_CFG_LENGTH 3

#define MPU6050_PWR1_DEVICE_RESET_BIT   7
#define MPU6050_PWR1_SLEEP_BIT          6
#define MPU6050_PWR1_CYCLE_BIT          5
#define MPU6050_PWR1_TEMP_DIS_BIT       3
#define MPU6050_PWR1_CLKSEL_BIT         2
#define MPU6050_PWR1_CLKSEL_LENGTH      3

#define TM_MPU6050_DataRate_8KHz       0   /*!< Sample rate set to 8 kHz */
#define TM_MPU6050_DataRate_4KHz       1   /*!< Sample rate set to 4 kHz */
#define TM_MPU6050_DataRate_2KHz       3   /*!< Sample rate set to 2 kHz */
#define TM_MPU6050_DataRate_1KHz       7   /*!< Sample rate set to 1 kHz */
#define TM_MPU6050_DataRate_500Hz      15  /*!< Sample rate set to 500 Hz */
#define TM_MPU6050_DataRate_250Hz      31  /*!< Sample rate set to 250 Hz */
#define TM_MPU6050_DataRate_125Hz      63  /*!< Sample rate set to 125 Hz */
#define TM_MPU6050_DataRate_100Hz      79  /*!< Sample rate set to 100 Hz */

#define TM_MPU6050_MOT_INT_EN          0x40
#define TM_MPU6050_DTA_RDY_INT         0x01
#define TM_MPU6050_DTA_RDY_MOTION_INT  0x41
#define TM_MPU6050_ACCEL_SENS_2G       0xE8
#define TM_MPU6050_ENABLE_DLPF         0x01
#define TM_MPU6050_PLL_XGYRO_CLOCK     0x01
#define TM_MPU6050_LP_PLL_XGYRO        0xA9
#define TM_MPU6050_LP_1_25_Hz          0x7
#define TM_MPU6050_INT_ACTIVE_HIGH     0b01010000
#define TM_MPU6050_INT_ACTIVE_LOW      0b11010000

#define BIT_I2C_MST_VDDIO   (0x80)
#define BIT_FIFO_EN         (0x40)
#define BIT_DMP_EN          (0x80)
#define BIT_FIFO_RST        (0x04)
#define BIT_DMP_RST         (0x08)
#define BIT_FIFO_OVERFLOW   (0x10)
#define BIT_DATA_RDY_EN     (0x01)
#define BIT_DMP_INT_EN      (0x02)
#define BIT_MOT_INT_EN      (0x40)
#define BITS_FSR            (0x18)
#define BITS_LPF            (0x07)
#define BITS_HPF            (0x07)
#define BITS_CLK            (0x07)
#define BIT_FIFO_SIZE_1024  (0x40)
#define BIT_FIFO_SIZE_2048  (0x80)
#define BIT_FIFO_SIZE_4096  (0xC0)
#define BIT_RESET           (0x80)
#define BIT_SLEEP           (0x40)
#define BIT_S0_DELAY_EN     (0x01)
#define BIT_S2_DELAY_EN     (0x04)
#define BITS_SLAVE_LENGTH   (0x0F)
#define BIT_SLAVE_BYTE_SW   (0x40)
#define BIT_SLAVE_GROUP     (0x10)
#define BIT_SLAVE_EN        (0x80)
#define BIT_I2C_READ        (0x80)
#define BITS_I2C_MASTER_DLY (0x1F)
#define BIT_AUX_IF_EN       (0x20)
#define BIT_ACTL            (0x80)
#define BIT_LATCH_EN        (0x20)
#define BIT_ANY_RD_CLR      (0x10)
#define BIT_BYPASS_EN       (0x02)
#define BITS_WOM_EN         (0xC0)
#define BIT_LPA_CYCLE       (0x20)
#define BIT_STBY_XA         (0x20)
#define BIT_STBY_YA         (0x10)
#define BIT_STBY_ZA         (0x08)
#define BIT_STBY_XG         (0x04)
#define BIT_STBY_YG         (0x02)
#define BIT_STBY_ZG         (0x01)
#define BIT_STBY_XYZA       (BIT_STBY_XA | BIT_STBY_YA | BIT_STBY_ZA)
#define BIT_STBY_XYZG       (BIT_STBY_XG | BIT_STBY_YG | BIT_STBY_ZG)

#define MPU6050_RA_MOT_DETECT_STATUS    0x61

#define MPU6050_MOTION_MOT_XNEG_BIT     7
#define MPU6050_MOTION_MOT_XPOS_BIT     6
#define MPU6050_MOTION_MOT_YNEG_BIT     5
#define MPU6050_MOTION_MOT_YPOS_BIT     4
#define MPU6050_MOTION_MOT_ZNEG_BIT     3
#define MPU6050_MOTION_MOT_ZPOS_BIT     2

/********************************/
#define BIT_CLK_INTERNAL_8MHZ 0x48
#define BIT_CLK_PLL_XGYRO 0x49
#define BIT_CLK_PLL_YGYRO 0x4A
#define BIT_CLK_PLL_ZGYRO 0x4B

#define BIT_ACC_RANGE_2G 0x0
#define BIT_ACC_RANGE_4G 0x8
#define BIT_ACC_RANGE_8G 0x10
#define BIT_ACC_RANGE_16G 0x18

#define BIT_GYRO_RANGE_250 0x0
#define BIT_GYRO_RANGE_500 0x8
#define BIT_GYRO_RANGE_1000 0x10
#define BIT_GYRO_RANGE_2000 0x18

#define ACC_REAL_VAL_PRESCALER_2G 16386
#define ACC_REAL_VAL_PRESCALER_4G 8192
#define ACC_REAL_VAL_PRESCALER_8G 4096
#define ACC_REAL_VAL_PRESCALER_16G 2048

#define GYRO_REAL_VAL_PRESCALER_250 131
#define GYRO_REAL_VAL_PRESCALER_500 65.5
#define GYRO_REAL_VAL_PRESCALER_1000 32.8
#define GYRO_REAL_VAL_PRESCALER_2000 16.4

/**
 * @brief  MPU6050 result enumeration
 */
typedef enum
{
    MPU6050_Result_Ok = 0x00,          // Everything OK
    MPU6050_Result_DeviceNotConnected, // There is no device with valid slave address
    MPU6050_Result_DeviceInvalid       // Connected device with address is not MPU6050
} MPU6050_Result_t;

typedef enum
{
    MPU6050_IS_NOT_MOVING = 0,
    MPU6050_IS_MOVING
} MPU6050_Flag;

/**
 * @brief  Main MPU6050 structure
 */
typedef struct
{
    uint16_t Gyro_Mult;         // Gyroscope corrector from raw data to "degrees/s"
    uint16_t Acce_Mult;         // Accelerometer corrector from raw data to "g"

    uint8_t Accelerometer_X_H; /*!< Accelerometer value X axis */
    uint8_t Accelerometer_X_L;
    uint8_t Accelerometer_Y_H; /*!< Accelerometer value Y axis */
    uint8_t Accelerometer_Y_L;
    uint8_t Accelerometer_Z_H; /*!< Accelerometer value Z axis */
    uint8_t Accelerometer_Z_L;
    uint8_t Gyroscope_X_H;     /*!< Gyroscope value X axis */
    uint8_t Gyroscope_X_L;
    uint8_t Gyroscope_Y_H;     /*!< Gyroscope value Y axis */
    uint8_t Gyroscope_Y_L;
    uint8_t Gyroscope_Z_H;     /*!< Gyroscope value Z axis */
    uint8_t Gyroscope_Z_L;
    uint8_t Temperature_H;       /*!< Temperature in degrees */
    uint8_t Temperature_L;

    uint8_t acc_range;
    uint8_t gyro_range;
} MPU6050_t;

typedef struct{
    uint8_t Address;         // I2C address of device
    I2C_HandleTypeDef *i2c;
} MPU6050_driver_typeDef;

MPU6050_driver_typeDef MPU6050_driver;


MPU6050_Result_t MPU6050_Init(I2C_HandleTypeDef *hi2c, MPU6050_t *DataStruct, uint8_t addr);
MPU6050_Result_t MPU6050_Init_ext(I2C_HandleTypeDef *hi2c, MPU6050_t *DataStruct, uint8_t addr, uint8_t clkSrc, uint8_t accelRange, uint8_t gyroRange);
/*Data gathering*/
MPU6050_Result_t MPU6050_getMotion6(MPU6050_t *DataStruct);
MPU6050_Result_t MPU6050_getTemperature(MPU6050_t *DataStruct);
MPU6050_Result_t MPU6050_getAcceleration(MPU6050_t *DataStruct);
MPU6050_Result_t MPU6050_getRotation(MPU6050_t *DataStruct);
/*IMU settings*/
MPU6050_Result_t setLowPower(uint8_t clkSrc);
MPU6050_Result_t MPU650_setIntLatched(int en);
MPU6050_Result_t MPU6050_setIntEnabled(int en);
MPU6050_Result_t MPU6050_setDLPF();
MPU6050_Result_t MPU6050_enableDataReadyInt();
MPU6050_Result_t MPU6050_enableMotionDetectionInt();
MPU6050_Result_t MPU6050_setHWInt();
MPU6050_Result_t MPU650_setMotionTreshold();
uint8_t MPU650_getMotionIntStatus();
MPU6050_Result_t MPU6050_setAccelRange(uint8_t range);
MPU6050_Result_t MPU6050_setGyroRange(uint8_t range);
MPU6050_Result_t MPU6050_setClockSource(uint8_t clkSrc);
MPU6050_Result_t MPU6050_setSamplingRate();
/*complementary*/
int16_t MPU6050_movingAverageFilter(MPU6050_t *DataStruct);
uint8_t MPU6050_getXPOS_MOT();
#endif
