#!/usr/bin/env python
import serial
import time
import datetime
from binascii import *
import time
#import matplotlib
#matplotlib.use('TkAgg')
#import numpy
from matplotlib import pyplot as plt
#from pylab import *
from drawnow import *

GLEN = 50
data_t=[0]*GLEN
data_y1=[0]*GLEN
data_y2=[0]*GLEN


    
#plt.ion() 
#ax1=plt.axes()
#line, = plt.plot(data_y1)
#plt.ylim([0,2])
 

def plot(ax, x, y, style):
	ax.xlimit([0,50])
	return ax.plot(x, y, style, animated=True)[0]

def kreslitko():
	plt.title('My Live Streaming Sensor Data')
	plt.plot(data_y1)
	

    
dataStartBits = [0x25, 0x72];
FRAME_LEN = 9
ACC_RANGE = [16384,8192,4096,2048]
#SHOW_GRAPH = False
SHOW_GRAPH = True
styles = ['r-', 'g-', 'y-', 'm-', 'k-', 'c-']

if SHOW_GRAPH == True:
	fig, axes = plt.subplots()
	fig.show()
	# We need to draw the canvas before we start animating...
	fig.canvas.draw()
	background = fig.canvas.copy_from_bbox(axes.bbox) 


	
tstart = time.time()
port = serial.Serial('/dev/ttyUSB0',115200, parity=serial.PARITY_NONE, stopbits=1, timeout=0.2)
port.flushInput()
port.flushOutput()



#plt.ion() 
#ax1=plt.axes()
#line, = plt.plot(data_y1)
#plt.ylim([0,2])


tik0 = time.time()
#while True:
for c in range(5000):
	bajt=port.read(1)

	if len(bajt)!=1:
		print ".",
		continue
	bajt=ord(bajt)
	
	if bajt == dataStartBits[0]:
		bajt=port.read(1)
		if len(bajt)!=1:
			print "-",
			continue
		bajt=ord(bajt)
		#print hex(bajt),
		if bajt == dataStartBits[1]:
			bajty=port.read(FRAME_LEN)
			if len(bajty)!=FRAME_LEN:
				print len(bajty),
				continue
			if ord(bajty[0]) > len(ACC_RANGE):
				continue
			tik = time.time()
			delta_t = tik - tik0
			acc_range = ACC_RANGE[ord(bajty[0])]
			acc_x = ord(bajty[1])*256+ord(bajty[2])
			acc_x = acc_range - acc_x
			acc_x /= float(acc_range)
			if acc_x < 0:
				acc_x = 4 + acc_x
			acc_y = ord(bajty[3])*256+ord(bajty[4])
			acc_y = acc_range - acc_y
			acc_y /= float(acc_range)
			data_t.append(c)
			data_y1.append(acc_x)
			data_y2.append(acc_y)
			
			acc_z = ord(bajty[5])*256+ord(bajty[6])
			acc_z = acc_range - acc_z
			acc_z /= float(acc_range)
			
			#if len(data_t)>GLEN:
			#	data_t = data_t[GLEN:]
			#	data_y1 = data_y1[GLEN:]
			#	data_y2 = data_y2[GLEN:]
			del data_t[0]
			del data_y1[0]
			del data_y2[0]
			print acc_x, "\t", acc_y, "\t", acc_z# delta_t
			
			
			##ymin = float(min(data_y1))-10
			##ymax = float(max(data_y1))+10
			##plt.ylim([ymin,ymax])
			#line.set_xdata(data_t)
			#line.set_ydata(data_y1)  # update the data
			#plt.draw() # update the plot

			if SHOW_GRAPH:
				fig.canvas.restore_region(background)
				#line = plot(axes, data_t, data_y1, styles[0])
				line = axes.plot(data_t, data_y1, styles[0], animated=False)[0]
				axes.draw_artist(line)
				fig.canvas.blit(axes.bbox)
			
			time.sleep(0.01)
			port.flushInput()
			port.flushOutput()
			tik0 = time.time()



print 'FPS:' , 2000/(time.time()-tstart)
